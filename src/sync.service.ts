import { Service, Context, Errors } from "moleculer";
import * as Base64 from "base-64";
import * as pako from "pako";

class BanNoteSyncService extends Service {
	protected timer: NodeJS.Timer;
	protected queue: {user: string, note: string, time: number, mod: string}[];

	constructor (broker) {
		super(broker);

		this.parseServiceSchema({
			name: "bot.banNoteSync",
			version: 1,
			dependencies: [
				{name: "reddit-rt.modlog", version: 1},
				{name: "reddit.wiki", version: 1}
			],
			actions: {
				syncNotes: {
					name: "syncNotes",
					handler: this.syncNotes
				}
			},
			events: {
				"reddit-rt.modlog.banuser": this.eventHandler
			},
			created: this.serviceCreated,
			// @ts-ignore
			started: this.serviceStarted
		});
	}

	async eventHandler(logEntry) {
		if(logEntry.description !== null) {
			this.logger.debug(`User ${logEntry.target_author} was banned with a reason, queueing...`);

			this.queue.push({
				user: logEntry.target_author,
				note: logEntry.description,
				time: logEntry.created_utc,
				mod: logEntry.mod
			});
		}
	}

	async syncNotes(ctx: Context) {
		this.logger.debug(`Queue length: ${this.queue.length}`);
		if(this.queue.length > 0) {
			this.logger.debug(`Updating user notes for ${this.queue.length} user(s)`);

			let usernotesPageContent = await this.broker.call("v1.reddit.wiki.getPage", {
				subreddit: process.env.BOT_BANNOTESYNC_SUBREDDIT,
				page: "usernotes"
			});
			usernotesPageContent = JSON.parse(usernotesPageContent.content_md);

			// Decode usernotes page
			let inflate = new pako.Inflate({to: "string"});
			inflate.push(Base64.decode(usernotesPageContent.blob));
			let usernotes = JSON.parse(inflate.result as string);

			for (let ban of this.queue) {
				let reason = {
					n: `Ban note: ${ban.note}`,
					t: ban.time,
					m: usernotesPageContent.constants.users.indexOf(ban.mod),
					l: "",
					w: usernotesPageContent.constants.warnings.indexOf("ban")
				};

				if(usernotes.hasOwnProperty(ban.user)) {
					usernotes[ban.user].ns.push(reason);
				} else {
					usernotes[ban.user] = {};
					usernotes[ban.user].ns = [reason];
				}
			}

			// Re-encode usernotes page
			let deflate = new pako.Deflate({to: "string"});
			deflate.push(JSON.stringify(usernotes), true);
			// @ts-ignore (this is how Toolbox does it so I know it works)
			usernotesPageContent.blob = Base64.encode(deflate.result);

			// Generate edit string
			let editReason = `Synchronizing ban notes for ${this.queue.map(ban => ban.user).join(", ")}`;

			// Save usernotes page to Reddit
			await this.broker.call("v1.reddit.wiki.setPage", {
				subreddit: process.env.BOT_BANNOTESYNC_SUBREDDIT,
				page: "usernotes",
				text: JSON.stringify(usernotesPageContent),
				reason: editReason
			});

			// Clear queue
			this.queue = [];

			this.logger.debug(`User notes updated successfully`);
		}
	}

	async serviceCreated() {
		this.queue = [];
	}

	async serviceStarted() {
		this.timer = setInterval(() => this.broker.call("v1.bot.banNoteSync.syncNotes"), Number(process.env.BOT_BANNOTESYNC_INTERVAL))
	}
}

module.exports = BanNoteSyncService