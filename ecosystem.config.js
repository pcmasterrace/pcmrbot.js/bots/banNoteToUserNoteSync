module.exports = {
	apps : [{
		name      : 'bot/banNoteToUserNoteSync',
		script    : 'build/sync.js',
		env: {
			// Either inject the values via environment variables or define them here
            TRANSPORT_BIND_ADDRESS: process.env.TRANSPORT_BIND_ADDRESS || "",
            BOT_BANNOTESYNC_SUBREDDIT: process.env.BOT_BANNOTESYNC_SUBREDDIT || "",
            BOT_BANNOTESYNC_INTERVAL: process.env.BOT_BANNOTESYNC_INTERVAL || ""
		}
	}]
};
